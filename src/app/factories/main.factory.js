(function() {
  'use strict';

  angular
    .module('inspinia')
    .factory('MainFactory', MainFactory);

  MainFactory.$inject = ['$http', 'BaseUrl'];
  function MainFactory($http, BaseUrl) {
    var service = {
      Profiles,
      Terminals
     };
    
    return service;

    ////////////////
    function Profiles(id) {
      return {
        get: () => $http.get(BaseUrl + '/v1/profile').then(complete),
        getById: id => $http.get(BaseUrl + `/v1/profile/${id}`).then(complete),
        create: postData => $http.post(BaseUrl + `/v1/profile`, postData).then(complete),
        remove: () => $http.delete(BaseUrl + '/v1/profile').then(complete),
        updateTerminal: (terminalId, postData) => $http.put(BaseUrl + `/v1/profile/${id}/terminal/${terminalId}`, postData).then(complete)
      }
    }
    function Terminals (id) {
      return {
        getByAffiliate: afiliate => $http.get(BaseUrl + `/v1/affiliate/${afiliate}/terminals`).then(complete),
        getLotes: postData => $http.post(BaseUrl + `/v1/terminal/${id}/lotes`, postData).then(complete)
      }
    }
  }
  function complete (res) {
    return res.data && res.data.message ? res.data.message : (res.data || res);
  }
})();