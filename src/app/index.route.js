(function() {
  'use strict';
  angular
    .module('inspinia')
    .config(routerConfig);
  /** @ngInject */
  function routerConfig($stateProvider, $urlRouterProvider, $locationProvider) {
    // $locationProvider.html5Mode({
    //   enabled: true,
    //   requireBase: true
    // }).hashPrefix('!');
    $stateProvider
      .state('index', {
        abstract: true,
        url: "/index",
        templateUrl: "/app/components/common/content.html"
      })
      .state('index.main', {
        url: "/main",
        templateUrl: "/app/main/main.html",
        data: { pageTitle: 'Módulo de conciliación' }
      })
      .state('index.main.configuration', {
        url: "/configuration",
        templateUrl: "/app/components/Configuration/configuration.html",
        controller: "ConfigurationController as vm",
        data: { pageTitle: 'Configuración' }
      })
      .state('index.main.query', {
        url: "/query/:id",
        templateUrl: "/app/components/Query/query.html",
        controller: "QueryController as vm",
        data: { pageTitle: 'Consultas' }
      })
      .state('index.main.profilelist', {
        url: "/profilelist",
        templateUrl: "/app/components/Profiles/ProfileList/profileList.html",
        controller: "ProfileListController as vm",
        data: { pageTitle: 'Lista de perfiles' }
      })
      .state('index.main.pendingpayments', {
        url: "/pendingpayments",
        templateUrl: "/app/components/PendingPayments/pendingPayments.html",
        data: { pageTitle: 'Consultas' }
      })
      .state('index.main.payments', {
        url: "/payments",
        templateUrl: "/app/components/Payments/payments.html",
        data: { pageTitle: 'Consultas' }
      })
      .state('index.main.lastclosures', {
        url: "/lastclosures",
        templateUrl: "/app/components/LastClosures/lastClosures.html",
        data: { pageTitle: 'Consultas' }
      })
      .state('index.main.terminal' , {
        url: "/terminal/:id",
        templateUrl: "/app/components/Terminal/terminalDetails.html",
        controller: 'TerminalDetailsController as vm',
        data: { pageTitle: 'Detalles del POS' }
      });
    $urlRouterProvider.otherwise('/index/main');
  }
})();
