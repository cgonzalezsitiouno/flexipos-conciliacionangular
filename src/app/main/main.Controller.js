(function() {
  'use strict';

  angular.module('inspinia')
    .controller('MainController', MainController);

  MainController.$inject = ["MainFactory", "ProfileService"];

  function MainController (MainFactory, ProfileService) {

    var vm = this;

    vm.breadCrumb = [
      {state: 'index.main.profilelist', label: 'profiles'}
    ]
    
    start();

    function start () {
      MainFactory.Profiles().get()
        .then(r => ProfileService.setProfiles(r))
        .catch(e => console.log("error", e));
      console.log(vm.breadCrumb)
    }
  }
})();
