(function() {
  'use strict';

  angular
    .module('inspinia')
    .service('BreadCrumbService', BreadCrumbService);

  function BreadCrumbService() {
    this.push = push;
    this.pop = pop;    
    
    ////////////////

    function push({label, state}) {
      if(!label || !state) {
        return;
      }
      if(!this._breadCrumb) {
        this._breadCrumb = [{label, state}]
        return;
      }
      this._breadCrumb.push({label, state});
    }
    function pop() {
      this._breadCrumb.pop();
    }
  }
})();