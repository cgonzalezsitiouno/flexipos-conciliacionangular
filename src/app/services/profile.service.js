(function() {
  'use strict';

  angular
    .module('inspinia')
    .service('ProfileService', ProfileService);

  function ProfileService() {
    let self = this;
    this.setProfile = setProfile;
    this.setProfiles = setProfiles;
    this.getProfile = getProfile;
    this.getProfileById = getProfileById;
    this.setTerminals = setTerminals;
    this.getTerminals = getTerminals;
    this.setTerminal = setTerminal;
    this.getTerminal = getTerminal;
    this.addProfile = addProfile;
    this.getProfiles = getProfiles;

    function addProfile (profile) {
      if (!self._profiles) {
        return self._profiles = [profile];
      }
      self._profiles.push(profile);
      console.log("Despues del update", self._profiles);
    }
    function setProfile(profile) { 
      self._profile = profile;
    }
    function getProfile() {
      return self._profile;
    }
    function setProfiles(profiles) { 
      self._profiles = profiles;
    }
    function getProfiles() {
      console.log("epa", self._profiles)
      return self._profiles;
    }
    function getProfileById(id) {
      if(!self._profiles)
        return null;
      return !id ? self._profiles[0] : self._profiles.filter(e => e._id === id);
    }
    function setTerminals(terminals){
      if(!self._profile)
        return null;
      self._profile.terminals = terminals;
    }
    function setTerminal (terminal) {
      self._terminal = terminal;
    }
    function getTerminal () {
      return self._terminal;
    }
    function getTerminals() {
      if(!self._profile)
        return null;
      return self.profile._terminals;
    }
  }
})();