(function () {
  'use strict';

  angular
    .module('inspinia')
    .directive('terminalList', terminalList);

  function terminalList() {
    var directive = {
      templateUrl: '/app/directives/terminal-list/terminalList.html',
      bindToController: true,
      controller: Controller,
      controllerAs: 'vm',
      link: link,
      restrict: 'E',
      scope: {
        terminals: "="
      }
    };
    return directive;

    function link(scope, element, attrs) {
    }
  }
  /* @ngInject */
  function Controller() {
    let vm = this;
    
    vm.updateSelected = updateSelected;

    function updateSelected(terminal) {
      terminal.selected = !terminal.selected;
    }
  }
})();