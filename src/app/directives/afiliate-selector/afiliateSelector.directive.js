(function () {
  'use strict';

  angular
    .module('inspinia')
    .directive('afiliateSelector', afiliateSelector);

  function afiliateSelector() {
    var directive = {
      templateUrl: "/app/directives/afiliate-selector/afiliateSelector.html",
      bindToController: true,
      controller: Controller,
      controllerAs: 'vm',
      link: link,
      restrict: 'E',
      scope: {
        onupdate: "&",
        action: "="
      }
    };
    return directive;

    function link(scope, element, attrs) {
      scope.$watch("vm.afiliado",function() {
        scope.vm.onupdate({p: scope.vm.afiliado})
      },1)
    }
  }
  /* @ngInject */
  function Controller() {
    let vm = this;
  }
})();