(function() {
  'use strict';

  angular
    .module('inspinia')
    .controller('ProfileListController', ProfileListController);

  ProfileListController.$inject = ['ProfileService','MainFactory','$state'];
  function ProfileListController(ProfileService, MainFactory, $state) {
    var vm = this;

    vm.selectProfile = selectProfile;
    vm.deleteProfiles = deleteProfiles;
    
    start();
    
    function start() {
      vm.profiles = ProfileService.getProfiles();
      if(!vm.profiles) {
        vm.loading = true;
        MainFactory.Profiles().get()
          .then(r => (vm.profiles = r) && (vm.loading = false))
          .catch(e => console.log("Error", e) || (vm.loading = false));
      }
    }
    function selectProfile(profile) {
      ProfileService.setProfile(profile);
      $state.go("index.main.query", {id: profile._id});
    }
    function deleteProfiles() {
      vm.loading = true;
      MainFactory.Profiles().remove()
        .then(r => {
          ProfileService.setProfiles([]);
          vm.loading = false;
          $state.go('index.main')
        })
        .catch(e => console.log("Error", e) || (vm.loading = false));
    }
  }
})();