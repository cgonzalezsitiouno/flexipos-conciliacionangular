(function() {
  'use strict';

  angular
    .module('inspinia')
    .controller('QueryController', QueryController);

  QueryController.$inject = ['ProfileService', 'MainFactory', '$state'];
  function QueryController(ProfileService, MainFactory, $state) {
    var vm = this;

    vm.selectTerminal = selectTerminal;
    activate();
    function activate() { 
      vm.loading = true;
      MainFactory.Profiles().getById($state.params.id)
        .then(([r]) => (vm.profile = r) && (vm.loading = false))
        .catch(e => console.log(e) || (vm.loading = false));
    }
    function selectTerminal(terminal) {
      if(terminal.adminPercent || terminal.userPercent) {
        vm.loading = true;
        MainFactory.Profiles($state.params.id).updateTerminal(terminal._id, {terminal})
          .then(r => {
            console.log("respuesta de UpdateTerminal", r, vm.profile);
            ProfileService.setProfile(vm.profile);
            vm.loading = false;
          });
      }
      ProfileService.setTerminal(terminal);
      $state.go("index.main.terminal",{id: terminal._id});
    }
  }
})();