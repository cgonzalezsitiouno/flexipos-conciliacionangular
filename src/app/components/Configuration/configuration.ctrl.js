(function() {
  'use strict';

  angular
    .module('inspinia')
    .controller('ConfigurationController', ConfigurationController);

  ConfigurationController.$inject = ["ProfileService", "MainFactory", "$state"];
  function ConfigurationController(ProfileService, MainFactory, $state) {
    console.log("ConfigurationCtrl")
    let vm = this;   

    vm.profile = {};
    vm.afiliado = false;
    vm.selectedTerminals = [];
    vm.updateQuery = updateQuery;
    vm.getAfiliateTerminals = getAfiliateTerminals;
    vm.canSave = canSave;
    vm.saveChanges = saveChanges;
    vm.addTerminals = addTerminals;

    activate();

    function activate() {

    }

    function addTerminals() {
      let terminalList = vm.terminals.filter(_selectedTerminals);
      [].push.apply(vm.selectedTerminals, terminalList);
      vm.terminals = [];
    }

    function updateQuery(query) {
      if(!query)
        return; 
      vm.afiliado = query.number;
    }
    function getAfiliateTerminals() {
      MainFactory.Terminals().getByAffiliate(vm.afiliado)
        .then(r => vm.terminals = r);       
    }
    function canSave () {
      return vm.afiliado && vm.selectedTerminals.length && vm.profile.name;
    }
    function saveChanges() {
      vm.profile.afiliado = vm.afiliado,
      vm.profile.terminals = vm.selectedTerminals      
      MainFactory.Profiles().create({Profile: vm.profile})
        .then(r => {
          console.log("creacion de perfil", r);
          ProfileService.addProfile(vm.profile);
          $state.go("index.main.profilelist");
        })
        .catch(e => console.log("Error", e));      
    }

    function _selectedTerminals(terminal) {
      return terminal.selected;
    }
  }
})();