(function() {
  'use strict';

  angular
    .module('inspinia')
    .controller('TerminalDetailsController', TerminalDetailsController);

  TerminalDetailsController.$inject = ['MainFactory', 'ProfileService', '$state'];
  function TerminalDetailsController(MainFactory, ProfileService, $state) {
    var vm = this;
    vm.terminal = ProfileService.getTerminal();
    vm.backToQuery = backToQuery;

    if(!vm.terminal)
      return $state.go('index.main.profilelist');
    activate();
    
    function activate() { 
      vm.loading = true;
      MainFactory.Terminals($state.params.id).getLotes({terminal: vm.terminal})
        .then(r => {
          vm.lotes = r.map(e => {
            let aux = e;
            aux.pUser = e.monto_total * vm.terminal.userPercent / 100;
            aux.pAdmin = e.monto_total * vm.terminal.adminPercent / 100;
            return aux;
          });
          vm.loading = false;
        })
        .catch(e => console.log("Error", e) || (vm.loading = false));
    }
    function backToQuery() {
      let profile = ProfileService.getProfile();
      console.log("Profile activo", profile);
      $state.go('index.main.query', {id: profile._id});
    }
  }
})();