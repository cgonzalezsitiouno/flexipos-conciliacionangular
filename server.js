'use strict';


//*********************************************************
//                       Dependencias
//*********************************************************
var express = require('express');
var compression = require('compression');
var app = express();
const bodyParser = require('body-parser');
var engine = require('consolidate');
var https = require('http');

//*********************************************************
//               Express Middlewares
//*********************************************************
app.use(compression());
// app.use(express.static(__dirname + '/public'));
app.use('/scripts', express.static(__dirname + '/dist/scripts'));
app.use('/styles', express.static(__dirname + '/dist/styles'));
app.use('/app', express.static(__dirname + '/src/app'));
app.use('/bower_components', express.static(__dirname + '/bower_components'));

app.engine('html', engine.mustache);
app.set('view engine', 'html');

app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ extended: true, limit: '50mb', parameterLimit: 50000 }));

//*********************************************************
//                       Index.html
//*********************************************************
app.get('/', function (req, res) {
  res.render(__dirname + '/src/index.html');
});

//*********************************************************
//      Permitir peticiones del cliente
//*********************************************************
app.use(function (req, res, next) {
  console.log("/******************/", req.method, req.url, req.origin);

  res.header("Access-Control-Allow-Origin", "*");
  res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE');
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, content-type, Accept, Authorization");
  next();
});

var server = https.createServer(app);

server.listen(80, function () {
  console.log('Servicio corriendo en el puerto 80');
});
